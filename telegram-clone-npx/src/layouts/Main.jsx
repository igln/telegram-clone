import React from "react";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import TelAppBar from '../public_components/app_bar/index'
import ChatItem from '../public_components/chat_item/index'
import testAvatar from "../assets/images/test_profile.jpeg"
import DesktopChatItem from "../public_components/chat_item/desktop";


class MainPage extends React.Component {
  render() {
    let chatArray = [];
    for (let i = 0; i < 10; i++) {
      chatArray.push(new ChatBrief("title " + (i + 1), testAvatar, "last msg", "mon"));
    }
    return (
        <div >
          <Container fixed maxWidth={"md"} style={{ height: "100vh"}}>
            <Grid container className="Container" >
              <Grid item xs={12} style={{background: "red"}}>
                <div><TelAppBar/></div>
              </Grid>
            </Grid>
            <Grid container className="Container" style={{ height: "60%"}}>
              <Grid item xs={4} style={{ height: "60%"}}>
                <div>
                  <div style={{background: "green"}}>search bar</div>
                   <div style={{background: "yellow", overflow: "auto", height: "50%"}}>
                     {chatArray.map((chat) =>
                         <ChatItem
                             title={chat.title}
                             imgSrc={chat.avatar}
                             lastMsg={chat.lastMsg}
                             lastMsgDay={chat.lastMsgDate}/>
                     )}
                 </div>
                </div>
              </Grid>
              <Grid item xs={8} style={{background: "blue", height: "90%"}}>
                main part
              </Grid>
            </Grid>
          </Container>
        </div>
    );
  }
}

class ChatBrief {
  title;
  avatar;
  lastMsg;
  lastMsgDate;


  constructor(title, avatar, lastMsg, lastMsgDate) {
    this.title = title;
    this.avatar = avatar;
    this.lastMsg = lastMsg;
    this.lastMsgDate = lastMsgDate;
  }
}

export default MainPage;
