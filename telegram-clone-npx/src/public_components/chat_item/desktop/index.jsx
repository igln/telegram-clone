import React, {createRef} from "react";
import Avatar from '@material-ui/core/Avatar';
import Grid from "@material-ui/core/Grid";
import Typography from '@material-ui/core/Typography';
import itemStyle from "../chatItem.module.css";
import ReactDOM from "react-dom";

function unselect() {
  console.log("unselect : " + ReactDOM.findDOMNode(this).id);
  this.compRootElement.setAttribute("mode", "unselected");
  this.setState({
    isSelected: false
  });
}

class DesktopChatItem extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {isSelected: false};
    unselect = unselect.bind(this);

  }

  componentDidMount() {
    this.compRootElement = ReactDOM.findDOMNode(this);
    this.compRootElement.setAttribute("mode", 'unselected');
  }

  itemClick() {
    //
    // let chatItems = document.querySelectorAll('.' + itemStyle.testClass);
    // chatItems.forEach(value => {
    //   if (value.getAttribute("mode") === "selected") {
    //     console.log(value);
    //   }
    // });
    // unselect();
    this.compRootElement.setAttribute("mode", "selected");


    this.setState({
      isSelected: true
    });
  }


  render() {
    return (
        <div style={{overflowX:"auto"}} className={itemStyle.testClass} onClick={() => this.itemClick()}>
          <Grid container className={itemStyle.box} style={{width: "300px"}}>
            <Grid item xs={2}>
              <Avatar alt="chat avatar" src={this.props.imgSrc} style={{width: "50px", height: "50px"}}/>
            </Grid>
            <Grid item xs={9} container style={{paddingLeft: "15px", marginTop: "-1px"}}>
              <Grid container style={{marginBottom: "2px"}}>
                <Typography noWrap variant={"subtitle2"} className={itemStyle.title}
                            style={{fontWeight: "700"}}>{this.props.title}</Typography>
              </Grid>
              <Grid container>
                <Typography noWrap variant={"caption"} className={itemStyle.lastMsg}>{this.props.lastMsg}</Typography>
              </Grid>
            </Grid>
            <Grid item xs={1} style={{textAlign: "end"}}>
              <Typography noWrap variant={"caption"} className={itemStyle.lastMsgDay}
                          style={{fontSize: ".85em"}}>{this.props.lastMsgDay}</Typography>
            </Grid>
          </Grid>
        </div>
    )
  }
}

export default DesktopChatItem;