import React from "react";
import {Hidden} from "@material-ui/core";
import DesktopChatItem from './desktop/index';
import MobileChatItem from './mobile/index';


class TelChatItem extends React.Component {
  render() {
    return (
        <div>
          <Hidden smDown>
              <DesktopChatItem  title={this.props.title} imgSrc={this.props.imgSrc} lastMsg={this.props.lastMsg} lastMsgDay={this.props.lastMsgDay}/>
          </Hidden>

          <Hidden mdUp>
            <MobileChatItem/>
          </Hidden>
        </div>
    );
  }
}

export default TelChatItem;