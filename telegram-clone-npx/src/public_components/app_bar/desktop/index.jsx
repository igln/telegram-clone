import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Grid from "@material-ui/core/Grid";

class DesktopAppBar extends React.Component {

  render() {
    return (
        <div>
          <Grid container className="Container">
            <Grid item xs={4}>
              <AppBar position={"relative"} style={{background: "#5682a3",boxShadow:"none"}}>
                <Toolbar>
                  this is left app bar
                </Toolbar>
              </AppBar>
            </Grid>
            <Grid item xs={8}>
              <AppBar position={"relative"} style={{background: "#5682a3",boxShadow:"none"}}>
                <Toolbar>
                  this is right app bar
                </Toolbar>
              </AppBar>            </Grid>
          </Grid>
        </div>


    )
  }
}

export default DesktopAppBar;