import React from "react";
import {Hidden} from "@material-ui/core";
import DesktopAppBar from './desktop/index';
import MobileAppBar from './mobile/index';

class TelAppBar extends React.Component {
  render() {
    return (
        <div>
          <Hidden smDown>
            <DesktopAppBar/>
          </Hidden>

          <Hidden mdUp>
            <MobileAppBar/>
          </Hidden>
        </div>
    )
  }
}

export default TelAppBar;