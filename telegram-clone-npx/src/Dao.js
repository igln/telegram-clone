import data from "./data/data.json";
import proto from "./data/proto.json";
import content from "./data/content.json";
import users from "./data/users.json";

class Dao {
    // Data
    getConversation(id) {
        return data[id]["chats"];
    }

    getBody(chat_id,id)
    {
        return data[chat_id]["chats"][id];
    }

    insertChat(user_id,body="",content=null,type=1)
    {
        var chat = {};
        chat['user_id'] = user_id;
        chat['body'] = body;
        chat['content'] = content;
        chat['type'] = type;
        chat['date'] = new Date().toDateString();
        var lastKey = Object.keys(data).sort().pop();
        lastKey = parseInt(lastKey);
        lastKey++;
        data['chats'][lastKey] = chat;
        return 200;
    }

    deleteChat(id)
    {
        if ( data['chats'][id] !== undefined && data['chats'][id] !== null)
        {
            data['chats'][id] == null;
            return 200;
        }else{
            return 500;
        }
    }

    // User
    getUser(id)
    {
        return users[id];
    }

    getName(id)
    {
        return users[id]['name'];
    }

    addUser(name="",type,mobile=null,content=null,description=null,invite_link=null)
    {
        var user = {};
            user["name"] = name;
            user["photo"] = content;
            user["date"] = new Date().toDateString();
            user["type"] = type;
            user["mobile"] = mobile;
            user["description"] = description;
            user["invite_link"] = invite_link;
        var lastKey = Object.keys(users).sort().pop();
        lastKey = parseInt(lastKey);
        lastKey++;
        users[lastKey] = user;
    }

    deleteUser(id)
    {
        if (users[id] != null && users[id] != undefined) {

            users[id] = null;
            return 200;
        }
        return 404;
    }

    // Types
    getStatusByNumber(n)
    {
        return proto['status'][n];
    }
    getChatTypeByNumber(n)
    {
        return proto['group_type'][n];
    }
    getUserTypeByNumber(n)
    {
        return proto['user_type'][n];
    }
    getMessageTypeByNumber(n)
    {
        return proto['status'][n];
    }

    // Content
    getContentById(id)
    {
        return content[id];
    }

    getLink(id)
    {
        return content[id][location];
    }

    addContent(path,mime=null)
    {
        var c = {};
        c["path"] = path;
        c["path"] = path.split('/')[path.split("/").length-1];
        c["mime_type"] = mime;
        c["is_enabled"] = 1;

        var lastKey = Object.keys(content).sort().pop();
        lastKey = parseInt(lastKey);
        lastKey++;
        content[lastKey] = c;
        return lastKey;
    }

    deleteContent(id)
    {
        if (content[id] !== undefined)
        {
            content[id]['is_enabled'] = 0;
        }
    }

}

export default Dao;
